# Accueil

Ce site recense tous les services informatiques proposés par Animath, pour chaque action.

Pour chaque service, on pourra trouver une description rapide de son utilité et de son fonctionnement, ainsi qu'une description plus détaillée de son installation, de sa configuration et de sa maintenance.

On trouvera [ici](Général) ce qui concerne généralement l'intégralité de l'architecture. Recensement des actions disposant d'un service sur le serveur :

 * [Services généraux Animath](Général)
 * [Correspondances des Jeunes Mathématicien·nes](Correspondances)
 * [Tournoi Français des Jeunes Mathématiciennes et Mathématiciens](TFJM²)
 * [Parlons-Maths]("Parlons-Maths")
 * [Animath International](International)
 * [Marrainage](Marrainage)
 * [Salon culture & jeux mathématiques](Salon)
 * [Filles Et Maths](Filles-Et-Maths)

D'autres services seront ajoutés à cette documentation prochainement.
