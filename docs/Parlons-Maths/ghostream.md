# Ghostream

!!! warning "Attention"
    Ghostream est toujours en développement et n'est pas encore prêt à être utilisé.

Ghostream est une plateforme de stream moderne très simple à déployer, incluant un lecteur dans une page Web et de l'authentification.

## Pourquoi Ghostream ?

Ghostream est un logiciel complètement open-source dévéloppé par le [Crans](https://crans.org). Le code source est accessible sur le [Gitlab du Crans](https://gitlab.crans.org/nounous/ghostream).

Il utilise le protocole de transmission très récent et libre [SRT](https://github.com/Haivision/srt), qui vient remplacer le vieux protocole RTMP encore massivement utilisé aujourd'hui bien que présentant de nombreux défauts.

Le protocole SRT permet d'assurer une latence extrêmement faible (de l'ordre de quelques centaines de millisecondes) tout en garantissant une expérience de stream optimale, en utilisant de plus moins de bande passante et un stream chiffré. Plus d'informations sur le protocole sur le [site d'Haivision](https://www.haivision.com/products/srt-secure-reliable-transport/).

Ghostream facilite de plus le déploiement du serveur en intégrant nativement une page Web contenant un lecteur vidéo, qui offre la possibilité de régler la qualité de diffusion pour les plus petites connexions.
Il est enfin possible de rediriger un flux vers un autre serveur de diffusion (tel que Twitch ou Youtube) si on veut faire de la diffusion simultanée. Ghostream a été pensé pour sa configuration.

## Installation

Ghostream s'installe très facilement via son image Docker incluse. Elle tourne sous Alpine Linux, une image sous Debian Bullseye est également incluse si besoin.

```yaml
  ghostream:
    build: sources/ghostream
    restart: always
    ports:
      - 9710:9710/udp
      - 10000-10005:10000-10005/udp
    volumes:
      - data/ghostream:/etc/ghostream:ro
      - /etc/localtime:/etc/localtime:ro
    labels:
      - "traefik.http.routers.ghostream.rule=Host(`stream.example.com`)"
      - "traefik.http.routers.ghostream.entrypoints=websecure"
      - "traefik.http.routers.ghostream.tls.certresolver=mytlschallenge"
      - "traefik.http.routers.ghostream.service=ghostream"
      - "traefik.http.services.ghostream.loadbalancer.server.port=8080"
```

Le port `9710` est le port entrant des connexions SRT, qui doit être ouvert en UDP car il s'agit du type de protocole que SRT utilise. Les ports 10000 à 10005 sont utilisés pour la connexion au lecteur Web. Allouer 6 ports permet de supporter 6 clients qui se connectent exactement au même moment sans attente, ce qui est largement suffisant.

Le dossier volumé `/etc/ghostream` doit contenir un fichier de configuration nommé `ghostream.yml`, détaillé plus bas.

Le lecteur vidéo est accessible sur le port 8080, ici configuré en reverse-proxy avec [Traefik](../Général/traefik).

À noter que Ghostream offre aussi un export de données Prometheus si besoin sur le port `2112`.

## Configuration

La configuration de Ghostream peut se faire soit par le biais d'un fichier de configuration situé dans `/etc/ghostream/ghostream.yml`, soit en indiquant via des variables d'environnement les variables de configuration à modifier.

Voici le fichier d'example de configuration :

```yaml
## Example Ghostream configuration ##
# Uncomment and edit to change values
#
# All settings can also be changed with an environnement variable,
# e.g. GHOSTREAM_AUTH_BACKEND=ldap will change auth.backend to "ldap"

## Authentification package ##
auth:
  # If you disable authentification no more check will be done on incoming
  # streams.
  #
  #enabled: true

  # Authentification backend,
  # can be "basic" to use a list of user:password
  # can be "ldap" to use a LDAP server
  #
  #backend: basic

  # Basic backend configuration
  # To generate bcrypt hashed password from Python, use:
  # python3 -c 'import bcrypt; print(bcrypt.hashpw(b"PASSWORD", bcrypt.gensalt(rounds=15)).decode("ascii"))'
  #
  #basic:
  #  credentials:
  #    # Demo user with password "demo"
  #    demo: $2b$15$LRnG3eIHFlYIguTxZOLH7eHwbQC/vqjnLq6nDFiHSUDKIU.f5/1H6

  # LDAP backend configuration
  #
  #ldap:
  #  uri: ldap://127.0.0.1:389
  #  userdn: cn=users,dc=example,dc=com

## Stream forwarding ##
# Forward an incoming stream to other servers
# The URL can be anything FFMpeg can accept as an stream output
forwarding:
  # By default nothing is forwarded.
  #
  # This example forwards a stream named "demo" to Twitch and YouTube,
  #demo:
  #  - rtmp://live-cdg.twitch.tv/app/STREAM_KEY
  #  - rtmp://a.rtmp.youtube.com/live2/STREAM_KEY

## Prometheus monitoring ##
# Expose a monitoring endpoint for Prometheus
monitoring:
  # If you disable monitoring module, no more metrics will be exposed.
  #
  #enabled: true

  # You should not expose monitoring metrics to the whole world.
  # To limit access to only localhost, use 127.0.0.1:2112
  #listenAddress: :2112

## SRT server ##
# The SRT server receive incoming stream and can also serve video to clients.
srt:
  # If you disable SRT module, it will be no more possible to receive incoming
  # streams and this whole app will become useless.
  #
  #enabled: true

  # To limit access to only localhost, use 127.0.0.1:9710
  #listenAddress: :9710

  # Max number of active SRT connections
  #maxClients: 64

## Web server ##
# The web server serves a WebRTC player.
web:
  # If you disable web module, the stream will be accessible only via SRT or
  # via forwarding module (see above).
  #
  #enabled: true

  # Custom CSS stylesheet to change theme
  #
  #customCss: ""

  # Web page favicon, can be .ico, .png or .svg
  #
  #favicon: /static/img/favicon.svg

  # Server name seen by users
  # If example.com reverses to this server, then change to example.com
  #
  #hostname: localhost

  # To limit access to only localhost, use 127.0.0.1:8080
  #
  #listenAddress: :8080

  # Web site name
  # Put something cool here, such as "Cr@ns Stream"
  #
  #name: Ghostream

  # Use the domain name as the stream name
  # e.g., on http://example.com:8080/ the stream served will be "example.com"
  # This implies that your domain will be able to serve only one stream.
  #
  #oneStreamPerDomain: false

  # Stream player poster
  # Shown when stream is loading or inactive.
  #
  #playerPoster: /static/img/no_stream.svg

  # Refresh period for viewer stats (below the player)
  #
  #viewersCounterRefreshPeriod: 20000

  # Add a web page as a side widget
  # This can be a public TheLounge or Element instance to make a chat.
  # You can use {{.Path}} to include current stream name,
  # e.g. https://example.com/stream_{{.Path}}
  #
  #widgetURL: ""

## WebRTC server ##
webrtc:
  # If you disable webrtc module, the web client won't be able to play streams.
  #
  #enabled: true

  # UDP port range used to stream
  # This range must be opened in your firewall.
  #
  #minPortUDP: 10000
  #maxPortUDP: 10005

  # STUN servers, you should host your own Coturn instance
  #
  #STUNServers:
  #  - stun:stun.l.google.com:19302
```

Dans le cas de Parlons-Maths, on peut placer le champ `oneStreamPerDomain` à `true` pour avoir le flux directement sur la page [stream.parlons-maths.fr](https://stream.parlons-maths.fr). Le widget est configuré pour avoir un chat [Matrix](matrix), sur la page [https://element.parlons-maths.fr/#/room/#chat:parlons-maths.fr](https://element.parlons-maths.fr/#/room/#chat:parlons-maths.fr).

## Streamer avec OBS

### Pré-requis

#### Ubuntu/Debian
Le protocole SRT étant encore très récent et peu supporté, il est possible que vous rencontriez certaines difficultés pour streamer depuis votre machine. Il est par exemple connu que sur Ubuntu 20.04 ou Debian Buster, la bibliothèque `libsrt` n'est pas installée et n'apparaît pas dans les dépôts officiels. Il est donc recommandé d'utiliser Ubuntu 20.10 ou Debian Bullseye/Sid, en installant le paquet `libsrt1-openssl` manuellement.

#### Arch Linux
Si vous utilisez Arch Linux, vous n'aurez normalement pas de problème, assurez-vous de bien avoir le paquet `srt` installé et à jour, normalement inclus avec `ffmpeg` qui est inclus avec `obs-studio`.

#### Alpine Linux
Sous Alpine Linux (utilisé notamment en tant que serveur pour tester Ghostream), les développeurs de Ghostream ont envoyé une modification dans les dépôts officiels afin d'ajouter le paquet `libsrt` et d'inclure cette bibliothèque dans l'installation de `ffmpeg`. Cependant, cela n'arrivera que dans la version 1.13 de Alpine Linux. Vous pouvez cependant d'ores-et-déjà récupérer ces paquets en allant dans les dépôts en avance : `apk add --no-cache -X https://dl-cdn.alpinelinux.org/alpine/edge/community/ ffmpeg libsrt`.

#### Windows/MacOS
Tant que vous maintenez OBS Studio à jour, vous ne devriez a priori pas rencontrer de difficulté : les versions les plus récentes d'OBS Studio incluent leur propre version de FFMPEG qui comprend la bibliothèque SRT.

### Comment streamer ?
OBS supporte le protocole SRT. Il vous suffit dans l'onglet « Stream (flux) » de choisir un service personnalisé, et d'entrer le serveur `srt://example.com:9710`, en remplaçant `example.com` par le bon nom de serveur.

La clé de stream doit être de la forme `id:password` : elle servira à vous authentifier sur le stream. Si `oneStreamPerDomain` vaut `false`, `id` sera l'identifiant du stream. Sinon, il s'agira du site sur lequel le stream sera actif.
Par exemple, si le stream est accessible sur le site `stream.example.com` et que le mot de passe est `PASSWORD`, il faudra rentrer `stream.example.com:PASSWORD` en clé de stream.

## Accéder au stream depuis un lecteur vidéo

### Depuis la page web

Si `id` votre clé de stream avec `oneStreamPerDomain = false`, rendez-vous sur `stream.example.com/id`. Si `oneStreamPerDomain = true`, alors l'id doit être `stream.example.com` et le stream sera accessible sur ce lien. Il s'agit de la façon la plus simple.

### Avec FFPlay

FFMPEG inclut son propre lecteur vidéo : `ffplay`. Il vous suffit de lancer `ffplay -fflags nobuffer srt://example.com:9710?streamid=id`. Les arguments `-fflags nobuffer` indiquent au lecteur de ne pas utiliser de tampon, ce qui réduit la latence en affichant les images dès qu'elles sont reçues. Par défaut, ffplay utilise un tampon de 5 secondes, vous pouvez le laisser si votre connexion n'est pas assez stable.

### Avec MPV

MPV ne supporte pas encore le protocole SRT, bien qu'une modification est en attente de validation pour ajouter ce support. D'ici quelques jours, il vous suffira d'exécuter `mpv srt://example.com:9710?streamid=id`, avec votre MPV bien à jour.

### Avec VLC Media Player

Bien que VLC supporte le protocole SRT, celui-ci n'accepte pas les options données. Il ne vous sera par conséquent pas possible de visionner une diffusion lancée sur Ghostream via VLC, à moins que l'id du stream soit vide.
