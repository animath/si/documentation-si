# Streamer sur animath.live

Cette page présente rapidement comment envoyer un stream sur l'une des chaînes d'animath.live en utilisant OBS. Elle complète les informations disponibles sur [animath.live](https://www.animath.live/doc_diffusion.php).

## OBS

OBS (pour Open Broadcast Studio) est un logiciel libre et open-source dédié à la capture d'écran et au stream. La documentation est disponible sur [le site d'OBS](https://obsproject.com/docs/).

Les paramètres à rentrer dans OBS (files>settings>stream) sont les suivants :

- Service : custom
- Server : `rtmp://stream.animath.live/liveX` (selon la chaîne sur laquelle on veut streamer)
- Stream Key : `stream?pwd=motdepasse`, avec le mot de passe défini dans la configuration Nginx.
- Use authentication : décoché.





*À venir : comptes diffuseureuses*.