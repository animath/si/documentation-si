# Correspondances des Jeunes Mathématicien·nes

Services utilisés :

* [Wordpress d'accueil](wordpress) ([correspondances-maths.fr](https://correspondances-maths.fr))
* [Plateforme](plateforme) ([inscription.correspondances-maths.fr](https://inscription.correspondances-maths.fr))
* [OSTicket](osticket) ([support.correspondances-maths.fr](https://support.correspondances-maths.fr))
* [Sympa](../Général/sympa) ([lists.correspondances-maths.fr](https://lists.correspondances-maths.fr))
