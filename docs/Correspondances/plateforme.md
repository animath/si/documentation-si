# Plateforme des Correspondances

Une plateforme faite maison (par Yohann <3) pour les Correspondances.

Lien : [inscription.correspondances-maths.fr](https://inscription.correspondances-maths.fr)

La plateforme va être intégralement recodée au cours de l'été, une documentation pour son installation suivra. L'installation devrait se faire de la même manière que celle de la [plateforme du TFJM²](../TFJM²/plateforme).
