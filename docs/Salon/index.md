# Salon Culture & Jeux Mathématiques

Services utilisés :

- [Base de données MySQL](mysql)
- [PHPMyAdmin](phpmyadmin)
- [Site Spip](spip) (édition 2020)
- [Plateforme de préinscription des exposant·es](preinscription)
- [Site Wordpress](wordpress) (édition 2021)

