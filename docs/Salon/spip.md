# SPIP

SPIP est un logiciel libre permettant la production de sites web, comme Wordpress. Les informations à son sujet sont disponibles sur [spip.net](https://spip.net).

## Installation

Nous avons utilisé l'image Docker développée par IPEOS (plus d'infos sur [Dockerhub](https://hub.docker.com/r/ipeos/spip/)). Pour sa mise en place, elle nécessite une base de données installée en amont (elle ne sera pas créée automatiquement). Dans le cas du site du salon, il s'agit d'une base de données [MySQL](mysql).

La configuration dans le `docker-compose.yml` est la suivante :

```yaml
spip:
    image: ipeos/spip:latest
    restart: always
    links:
       - mysql
    networks:
       - salon-maths
    env_file:
       - /srv/Salon-Maths/secrets/spip.env
    volumes:
       - "/srv/Salon-Maths/data/spip:/var/www/html"
    labels:
      - "traefik.http.routers.salon-maths.rule=Host(`salon-math.fr`)"
      - "traefik.http.routers.salon-maths.entrypoints=websecure"
      - "traefik.http.routers.salon-maths.tls.certresolver=mytlschallenge"
```



Les variables d'environnement permettent notemment d'accéder à la base de données :

```bash
SPIP_DB_SERVER=mysql #nom du container MySQL
SPIP_DB_LOGIN=spip
SPIP_DB_PASS=MySecurePassword
SPIP_DB_NAME=spip
```

D'autres variables d'environnement existent (détails sur [la documentation de l'image](https://hub.docker.com/r/ipeos/spip/)), mais ne sont pas utiles ici. 

Le dossier `/var/www/html` appartient à `www-data:www-data` et contient l'ensemble des éléments (images, documents, thèmes...) utilisés par le site web. 

En cas de migration d'un SPIP existant, il faudra réactiver à la main tous les plugins *via* l'interface admin de Spip.
