# PHPMyAdmin

PHPMyAdmin est une plateforme Web sur laquelle gérer la base de données MySQL.

Sa documentation est disponible à l'adresse : [phpmyadmin.net](https://www.phpmyadmin.net/).

Un accès est disponible pour le TFJM² à l'adresse dbadmin.salon-math.fr.

## Installation

PHPMyAdmin s'installe avec son image Docker officielle :

```yaml
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    links:
      - database:db
    env_file:
      - /srv/secrets/phpmyadmin.env
    restart: always
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
    labels:
      - "traefik.http.routers.pma.rule=Host(`dbadmin.example.com`)"
      - "traefik.http.routers.pma.entrypoints=websecure"
      - "traefik.http.routers.pma.tls.certresolver=mytlschallenge"
```

Si on veut restreindre l'accès par mot de passe Traefik, on ajoute le label :

```
      - "traefik.http.routers.pma.middlewares=auth"
```

PHPMyAdmin est déjà prêt à être lancé. Toutefois, on peut souhaiter se connecter automatiquement à la base de données, afin de n'utiliser que l'authentification de Traefik, en remplissant les variables d'environnement :

```
PMA_USER=root
PMA_PASSWORD=
```

De la sorte, se connecter à `https://dbadmin.example.com` ouvre directement PHPMyAdmin.

La mise à jour du service se fait simplement en mettant à jour l'image Docker : `docker-compose pull phpmyadmin && docker-compose up -d phpmyadmin`.