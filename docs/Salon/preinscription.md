# Plateforme de préinscription

Il existe une plateforme de pré-inscription pour les exposant·e·s du salon (édition 2021). Il s'agit d'un formulaire ([preinscription.salon-math.fr](preinscription.salon-math.fr)) envoyant les infos à la bdd (container [mysql](mysql)). 

## Installation

La plateforme de préinscription est écrite en PHP et communique avec la base de données en utilisant le pilote [PDO_MySQL](https://www.php.net/manual/fr/ref.pdo-mysql.php). Comme ce pilote n'est pas inclus dans l'image officielle PHP/Apache, nous sommes passées par une image docker "maison" très simple, disponible dans le fichier `preinscription.dockerfile`:

```bash
FROM php:7.4-apache
COPY ./data/preinscription/ /var/www/html/
WORKDIR /var/www/html
RUN docker-php-ext-install pdo pdo_mysql
```

On utilise simplement l'image php officielle, dans laquelle on installe PDO (instruction `RUN`).

Le fichier `docker-compose.yml` se configure alors de la façon suivante :

```YAML
preinscription:
    build : 
       context: .
       dockerfile: preinscription.dockerfile
    depends_on:
      - mysql
    links:
      - mysql
    volumes:
       - "/srv/Salon-Maths/data/preinscription:/var/www/html"
    stdin_open: true
    tty: true
    networks:
      - salon-maths
    labels:
      - "traefik.http.routers.preinscription-salon-maths.rule=Host(`preinscription.salon-math.fr`)"
      - "traefik.http.routers.preinscription-salon-maths.entrypoints=websecure"
      - "traefik.http.routers.preinscription-salon-maths.tls.certresolver=mytlschallenge"
```

