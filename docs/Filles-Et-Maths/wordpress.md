# Wordpress

Wordpress est un système de gestion de contenu, permettant de lancer un site Web simplement.

Plus de détails sur [wordpress.org](https://fr.wordpress.org/).

## Installation

Wordpress dispose d'une image Docker nommée `wordpress` bien conçue. Sa configuration dans le `docker-compose.yml` :

```yaml
  wordpress:
    image: wordpress
    links:
      - database
    restart: always
    volumes:
      - "/srv/data/wordpress/wp-content:/var/www/html/wp-content"
      - "/etc/localtime:/etc/localtime:ro"
    env_file:
      - /srv/secrets/wordpress.env
    labels:
      - "traefik.http.routers.wordpress.rule=Host(`example.com`)"
      - "traefik.http.routers.wordpress.entrypoints=websecure"
      - "traefik.http.routers.wordpress.tls.certresolver=mytlschallenge"
```

Et sa liste de variables d'environnement :

```
WORDPRESS_DB_USER=
WORDPRESS_DB_HOST=database
WORDPRESS_DB_NAME=
WORDPRESS_DB_PORT=
WORDPRESS_DB_PASSWORD=
WORDPRESS_TABLE_PREFIX=aninter_
```

La variable `WORDPRESS_TABLE_PREFIX` a été rajoutée car le préfixe de la base données wordpress n'était pas `wp_` (ce qui est le cas par défaut) mais `mod910_`.

La base de données doit nécessairement est une base de données de type MySQL.

Le dossier `/var/www/html/wp-content` doit appartenir à `www-data:www-data`. Il contient l'ensemble des éléments envoyés, tels que les documents PDF à télécharger par exemple, ou encore les thèmes et extensions.

Une fois cela fait, le service peut déjà être lancé : `docker-compose up -d wordpress`. Il suffit d'accéder à `https://example.com` pour créer le premier compte.

!!! info "Bon à savoir"
	Pour se connecter à l'interface d'édition, il faut se rendre sur la page `/wp-admin`.


## Mise à jour

La mise à jour de Wordpress est très simple puisqu'il suffit d'exécuter `docker-compose pull wordpress` et de redémarrer le service.

Il est inutile d'essayer de mettre à jour Wordpress depuis l'interface administrateur puisque le stockage n'est pas persistant. En revanche, c'est sur cette interface qu'il faut mettre à jour les thèmes et extensions.
