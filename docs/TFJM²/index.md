# TFJM²

Services utilisés :

* [Wordpress d'accueil](wordpress) ([tfjm.org](https://tfjm.org))
* [Plateforme](plateforme) ([inscription.tfjm.org](https://inscription.tfjm.org))
* [Owncloud](cloud) ([cloud.tfjm.org](https://cloud.tfjm.org))
* [Doc](doc) ([doc.tfjm.org](https://doc.tfjm.org))
* [Gitea](gitea) ([git.tfjm.org](https://git.tfjm.org))
* [Limesurvey](limesurvey) ([survey.tfjm.org](https://survey.tfjm.org))
* [Lychee](lychee) ([photos.tfjm.org](https://photos.tfjm.org))
* [Organiser un TFJM²](organisation) ([organiser-un.tfjm.org](https://organiser-un.tfjm.org))
* [OSTicket](osticket) ([support.tfjm.org](https://support.tfjm.org))
* [PHPMyAdmin](phpmyadmin) ([dbadmin.tfjm.org](https://dbadmin.tfjm.org))
* [Portainer](portainer) ([portainer.tfjm.org](https://portainer.tfjm.org))
* [Forum des problèmes](problemes) ([problemes.tfjm.org](https://problemes.tfjm.org))
* [Listes de diffusion SYMPA](../Général/sympa) ([lists.tfjm.org](https://lists.tfjm.org))
* [Tirages](tirages) ([tirages.tfjm.org](https://tirages.tfjm.org))
* [Wiki](wiki) ([wiki.tfjm.org](https://wiki.tfjm.org))
* [MySQL](mysql)
* [PostgreSQL](postgresql)
