# Plateforme du TFJM²

Une plateforme a été faite maison par pour le TFJM². Elle permet l'inscription des équipes, le dépôt des documents d'autorisation, et le dépôt des solutions et notes de synthèse.

La plateforme est accessible ici : [inscription.tfjm.org](https://inscription.tfjm.org)

Les 

Une documentation plus pratique sur son utilisation arrivera ultérieurement.

## Installation

L'installation se fait via une image Docker bien conçue :

```yaml
 inscription-tfjm:
    build: /srv/sources/inscription-tfjm
    links:
      - postgres
    env_file:
      - /srv/secrets/inscription-tfjm.env
    restart: always
    volumes:
      - /srv/data/inscription-tfjm/media:/code/media
      - "/etc/localtime:/etc/localtime:ro"
    labels:
      - "traefik.http.routers.inscription-tfjm.rule=Host(`inscription.tfjm.org`)"
      - "traefik.http.routers.inscription-tfjm.entrypoints=websecure"
      - "traefik.http.routers.inscription-tfjm.tls.certresolver=mytlschallenge"
```

Les variables d'environnement à rentrer étant :

```
TFJM_STAGE=                     # dev ou prod
TFJM_YEAR=2021                  # Année de la session du TFJM²
DJANGO_DB_TYPE=                 # MySQL, PostgreSQL ou SQLite (par défaut)
DJANGO_DB_HOST=                 # Hôte de la base de données
DJANGO_DB_NAME=                 # Nom de la base de données
DJANGO_DB_USER=                 # Utilisateur de la base de données
DJANGO_DB_PASSWORD=             # Mot de passe pour accéder à la base de données
SMTP_HOST=                      # Hôte SMTP pour l'envoi de mails
SMTP_PORT=465                   # Port du serveur SMTP
SMTP_HOST_USER=                 # Utilisateur du compte SMTP
SMTP_HOST_PASSWORD=             # Mot de passe du compte SMTP
FROM_EMAIL=contact@tfjm.org     # Nom de l'expéditeur des mails
SERVER_EMAIL=contact@tfjm.org   # Adresse e-mail expéditrice
```

Le champ `TFJM_STAGE` spécifie si l'instance installée est une instance de développement ou de production. En développement, il est possible d'ajouter le dossier `/code` en volume afin de pouvoir modifier plus facilement les fichiers. De plus, le serveur de développement est lancé par Django et dispose d'un système de détection de modifications de fichiers, relançant le serveur Web automatiquement.

## Mise à jour

En développement, il suffit de ... modifier les fichiers pour mettre à jour.

En production, la mise à jour se fait via Git, puis il suffit de reconstruire l'image :

```bash
cd /srv/sources/inscription-tfjm
git pull
cd /srv/upgrade
docker-compose -f /srv/docker/docker-compose.yml up -d --build inscription-tfjm
```

La base de données sera mise à jour automatiquement si nécessaire.
