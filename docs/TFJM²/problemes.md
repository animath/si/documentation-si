# Vanilla Forums

Vanilla Forums est un service proposant un forum. Il est utilisé par exemple pour discuter des problèmes du TFJM² et des Correspondances des Jeunes Mathématicien·nes pendant leur conception, à l'adresse [problemes.tfjm.org](https://problemes.tfjm.org).

Pour plus d'informations : [vanillaforums.com/fr](https://vanillaforums.com/fr/)

## Installation

Vanilla Forums ne dispose malheureusement pas d'image Docker officielle. Ses sources sont accessibles ici : [github.com/vanilla/vanilla](https://github.com/vanilla/vanilla)

Une image Docker maison a alors été créée, qu'on peut trouver ici : [gitlab.com/animath/si/vanilla](https://gitlab.com/animath/si/vanilla)

Le fichier `docker-compose.yml` se configure alors de la façon suivante :

```yaml
  forum:
    build: /srv/sources/vanilla-forums
    restart: always
    links:
      - database
    volumes:
      - '/srv/data/forum/conf:/var/www/html/conf'
      - '/srv/data/forum/locales:/var/www/html/locales'
      - '/srv/data/forum/themes:/var/www/html/themes'
      - '/etc/localtime:/etc/localtime:ro'
    env_file:
      - /srv/secrets/forum.env
    labels:
      - "traefik.http.routers.forum.rule=Host(`forum.example.com`)"
      - "traefik.http.routers.forum.entrypoints=websecure"
      - "traefik.http.routers.forum.tls.certresolver=mytlschallenge"
```

Avec ses variables d'environnement, configurant la connexion avec la base de données :

```
MYSQL_HOST=database
MYSQL_USER=
MYSQL_DATABASE=
MYSQL_PASSWORD=
```

Le service est désormais prêt à être lancé. La première connexion au site configurera le compte initial.

La configuration se trouve dans `/var/www/html/conf/config.php`. Les volumes doivent appartenir à `www-data:www-data`.

## Mise à jour

La mise à jour se fait depuis Git. Un script est présent dans `/srv/upgrade` :

```bash
cd /srv/sources/vanilla-forums
git pull origin-vanilla master
git push
docker-compose -f /srv/docker/docker-compose.yml up -d --build forum
```

Il faut ensuite visiter la page `https://forum.example.com/utility/update` pour terminer la mise à jour.
