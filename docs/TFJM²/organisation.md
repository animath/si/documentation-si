# Organiser un TFJM²

Ce site a pour objectif de regrouper toutes les informations nécessaires concernant l'organisation du TFJM², à l'échelle locale ou nationale. Il est accessible à l'adresse [organiser-un.tfjm.org](https://organiser-un.tfjm.org).

Ce site est écrit en Markdown, comme cette présente [documentation](doc), dont les sources sont disponibles sur [un dépôt Gitlab](https://gitlab.com/animath/si/organiser-un.tfjm.org/), et compilées en un site Web statique à l'aide de [MKDocs Material](https://squidfunk.github.io/mkdocs-material/).

Une image Docker a été conçue à la main permettant de mettre à jour automatiquement un site compilé de la sorte à partir d'un dépôt Git, dès lors qu'une action de push est réalisée. Les sources de cette image sont accessibles ici : [gitlab.com/animath/si/mkdocs-server-from-git](https://gitlab.com/animath/si/mkdocs-server-from-git), et l'image Docker peut être trouvée sur DockerHub sous le nom `ynerant/mkdocs-server-from-git`.

## Installation

L'installation se fait en deux temps. On considère ici une installation générale sur le site `site.example.com` du dépôt `https://git.example.com/user/repo.git`, qui s'adapte très facilement à la configuration du site d'organisation.

Tout d'abord, il faut préparer le service. Dans le fichier `docker-compose.yml` :

```yaml
  site:
    image: ynerant/mkdocs-server-from-git
    restart: always
    environment:
      - MKDOCS_SERVER_GIT_URL=https://git.example.com/user/repo.git
    labels:
      - "traefik.http.routers.site.rule=Host(`site.example.com`)"
      - "traefik.http.routers.site.entrypoints=websecure"
      - "traefik.http.routers.site.tls.certresolver=mytlschallenge"
```

Il n'y a pas d'autre variable d'environnement à définir, le service est déjà prêt à être lancé. En se rendant sur `https://site.example.com`, il est normalement déjà possible d'admirer le site compilé.

Dans un second temps, il faut configurer la mise à jour à chaque action push. Pour cela, il suffit de configurer un nouveau webhook sortant dans le dépôt contenant les sources Markdown, pointant sur l'adresse `https://site.example.com/trigger-ci.json`. Et c'est tout ! Essayez un nouveau push, réactualisez la page et admirez.

Il est à noter que le dépôt des sources du site doit impérativement contenir un fichier `requirements.txt`, qui contient l'ensemble des paquets PIP nécessaires à installer. Par défaut, seuls `mkdocs` et `mkdocs-material` sont installés. Attention : si vous ajoutez de nouveaux paquets, vous devrez redémarrer le service (via un simple `docker-compose restart site`) pour que l'installation des paquets PIP prenne effet.

Il n'y a a priori pas de mise à jour de l'image à faire, mais s'il y en a, un simple `docker-compose pull site` suffira.
