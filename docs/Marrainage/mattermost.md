# Mattermost

[Mattermost](https://mattermost.com/) est un service de discussion instantanée. C'est le concurrent open-source de Slack. La documentation est accessible ici : https://docs.mattermost.com/.

Le mattermost du marrainage est disponible ici : https://discute.animath.live



## Installation

Mattermost a une image docker, ce qui facilite l'installation. Pour fonctionner, on a aussi besoin d'un système de gestion de données (ici, postgres) :

```yaml
db:
    image: postgres:13
    # read_only: true
    restart: unless-stopped
    volumes:
      - /srv/Marrainage/data/postgresql:/var/lib/postgresql/data
      - /etc/localtime:/etc/localtime:ro
    environment:
      - POSTGRES_USER=marrainage
      - POSTGRES_PASSWORD=myPassword
      - POSTGRES_DB=marrainage
    networks:
      - marrainage
```

Le volume `var/lib/postgresql/data` doit appartenir à `postgresql:postgresql` et avoir les permissions `0700` pour les dossiers et `0600` pour les exécutables. Les variables d'environnement sont celles qui seront utilisées par Mattermost.



```yaml
 mattermost:
    image: mattermost/mattermost-team-edition
    restart: unless-stopped
    links:
      - "db"
    volumes:
      - /srv/Marrainage/data/mattermost/config:/mattermost/config:rw
      - /srv/Marrainage/data/mattermost/data:/mattermost/data:rw
      - /srv/Marrainage/data/mattermost/logs:/mattermost/logs:rw
      - /srv/Marrainage/data/mattermost/plugins:/mattermost/plugins:rw
      - /srv/Marrainage/data/mattermost/client-plugins:/mattermost/client/plugins:rw
      - /etc/localtime:/etc/localtime:ro
    environment:
      - MM_USERNAME=marrainage
      - MM_PASSWORD=myPassword
      - MM_DBNAME=marrainage
      - MM_SQLSETTINGS_DATASOURCE=postgres://marrainage:myPassword@db:5432/mattermost?sslmode=disable&connect_timeout=10
    networks:
      - marrainage
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.mattermost.rule=Host(`discute.animath.live`)"
      - "traefik.http.routers.mattermost.entrypoints=websecure"
      - "traefik.http.routers.mattermost.tls.certresolver=mytlschallenge"
      - "traefik.http.routers.mattermost.service=mattermost"
      - "traefik.http.services.mattermost.loadbalancer.server.port=8065"
```

Les variables d'environnement permettent de se connecter à la base de données PostgreSQL.

Il faut modifier le propriétaire des volumes `/mattermost/*` avec un `chown -R 2000:2000`.



## Configuration SMTP

Afin d'assurer la réception des mails (notifications, perte de mot de passe, ...), il faut configurer le SMTP. Cela peut se faire graphiquement à partir de la "system console" de Mattermost (3 lignes à côté du nom de l'équipe > System Console), dans Environment>SMTP (cf [la page de documentation dédiée](https://docs.mattermost.com/install/smtp-email-setup.html)). Les paramètres à choisir sont, pour Google Teams :

- SMTP Server : smtp.gmail.com
- SMTP Server Port : 587
- Enable SMTP Authentication : true
- SMTP Server Username : noreply@animath.fr
- Connection Security : SARTTLS
- Skip Server Certificate Verification : false

