# Sympa

Sympa est l'acronyme de `SYstème de Multi-Postage Automatique`, et propose un service de listes de diffusion. Le projet est géré par la `Sympa community`, initialement gérée par des français, et est disponible sur Github : [https://github.com/sympa-community/sympa](https://github.com/sympa-community/sympa).

Sympa est accessible sur différentes URL, pour chaque service nécessitant des listes de diffusion : [https://lists.tfjm.org](https://lists.tfjm.org), [https://lists.correspondances-maths.fr](https://lists.correspondances-maths.fr).

## Installation

Comme tous les services présents sur le serveur, Sympa s'installe via une image Docker faite maison : [https://gitlab.com/animath/si/sympa-docker](https://gitlab.com/animath/si/sympa-docker). Cette image installe Sympa avec un serveur mail Postfix très peu configuré et un serveur web Nginx.

L'image a été conçue de sorte à ce que la configuration initiale soit très simple pour un service pourtant compliqué :

```yaml
  sympa:
    build: /srv/sources/sympa
    hostname: lists.animath.fr
    links:
      - postgres
    ports:
      - "25:25"
      - "465:465"
    volumes:
      - /srv/data/sympa/spool:/var/spool/sympa
      - /srv/data/sympa/lib:/var/lib/sympa
      - /srv/data/sympa/transport:/etc/sympa/transport
      - /srv/data/sympa/robots:/etc/sympa/robots
    environment:
      MAIN_LIST_DOMAIN: "lists.animath.fr"
      DOMAINS: "lists.animath.fr;lists.tfjm.org;lists.correspondances-maths.fr"
      LISTMASTERS: "yohann.danello AT animath.fr"
      DB_TYPE: PostgreSQL
      DB_HOST: postgres
      DB_PORT: 5432
      DB_NAME: sympa
      DB_USER: sympa
      DB_PASSWORD: sympa
    labels:
      - "traefik.http.routers.sympa.rule=Host(`lists.tfjm.org`, `lists.correspondances-maths.fr`)"
      - "traefik.http.routers.sympa.entrypoints=websecure"
      - "traefik.http.routers.sympa.tls.certresolver=mytlschallenge"
      - "traefik.http.services.sympa.loadbalancer.server.port=80"
```

En remplaçant `AT` par `@`.

On suppose que `/srv/sources/sympa` contient un clône du dépôt ci-dessus, et que `/srv/data` est le dossier contenant les volumes Docker.

Le dossier `/etc/sympa/transport` contient un fichier `sympa_transport` avec la liste des adresses mail d'arrivée. Il est généré automatiquement et n'est pas à toucher manuellement, mais il doit être préservé.

Le dossier `/var/spool/sympa` contient la liste d'attente des mails à envoyer, comprenant les mails en attente de modération. Ce n'est donc pas une information à perdre.

Le dossier `/var/lib/sympa` contient les paramètres de chaque liste. Ce n'est à nouveau pas à modifier manuellement, il vaut mieux passer par l'interface Web.

Enfin, le dossier `/etc/sympa/robots` contient l'ensemble des fichiers de configurations propre à chaque sous-domaine. Eux peuvent être modifiés, et viennent remplacer les paramètres de Sympa par défaut. Un exemple de configuration :

```
wwsympa_url		https://lists.tfjm.org/sympa
title			Listes de diffusion pour le TFJM²
listmaster		contact AT tfjm.org
```

Le champ `hostname` est facultatif. De même, la variable d'environnement `MAIN_LIST_DOMAIN` doit contenir le nom de domaine principal, mais celui-ci ne sera pas utilisé ailleurs que dans la configuration de base de Sympa.

La variable d'environnement `DOMAINS` contient l'ensemble des noms de domaine supportés par Sympa, séparés par des points-virgule. La configuration initiale est ensuite générée automatiquement dès que l'image démarre. Attention : il n'est pas simple de supprimer un nom de domaine. L'ensemble des noms de domaine rentrés ici doit être rigoureusement identique à l'ensemble des URL auxquelles on peut accéder à Sympa.

La variable d'environnement `LISTMASTERS` doit contenir l'ensemble des adresses mail des utilisateurs qui auront tous les droits sur toutes les listes, sur tous les noms de domaine, séparées par des virgules. Elle ne doit comporter que des responsables informatique. Pour ajouter des listmasters sur un domaine spécifique, par exemple un·e président·e d'une action, il faut ajouter les adresses mail dans un champ `listmaster` dans le fichier correspondant dans `/srv/data/sympa/robots`.

Toutes les autres variables d'environnement concernent l'accès à la base de données. Les derniers labels concernent Traefik.

## Configuration des DNS

Même si à ce stade Sympa est prêt à être utilisé, il y a de grandes chances pour que les mails envoyés arrivent en spam. Il faut alors bien configurer ses DNS afin de bien authentifier ses mails.

### Le Reverse-DNS

La variable d'environnement `MAIN_LIST_DOMAIN` doit impérativement contenir le DNS principal de votre serveur, aussi appelé *reverse DNS* (rDNS). Lors de la réception d'un mail, le client va comparer l'hôte signalé et l'IP effective. Si les informations diffèrent, il y a de grandes chances que le mail soit détecté comme du spam.

Vous pouvez récupérer l'adresse IP de votre machine en exécutant `host dns`. Par exemple, `host tfjm.org` renvoie `tfjm.org has address 164.132.58.71`. En entrant cette fois-ci une IP, on récupère le rDNS associé à cette IP. Par exemple, `host 164.132.58.71` renvoie `71.58.132.164.in-addr.arpa domain name pointer tfjm.org` : le rDNS est bien configuré.

Si le nom de domaine inverse associé n'est pas bon, il faut le changer dans les paramètres de la machine.

!!! warning ""
	Cet ajout n'est à faire uniquement pour l'hôte principal.

### L'enregistrement MX

Vous devez déclarer un enregistrement MX dans les zones DNS pour chaque couple domaine/sous-domaine, indiquant qu'un serveur mail est présent. Par exemple :

```
lists IN MX 1 lists.example.com.
```

La priorité est ici de 1, cela est selon les besoins.

### Le champ DMARC

Vous devez créer un nouvel enregistrement TXT pour chaque adresse sur le sous-domaine `_dmarc.<sous-domaine>` contenant :

```
_dmarc.lists IN TXT "v=DMARC1; p=none"
```

### L'enregistrement SPF

Enfin, il faut ajouter un enregistrement SPF pour chaque domaine/sous-domaine :

```
lists IN TXT "v=spf1 a mx ip4:XX.XX.XX.XX ~all"
```

!!! warning ""
	À venir : la signature DKIM et la correction de l'erreur "SPF: HELO does not match SPF record (softfail)" afin d'arriver à un  score de 10/10

!!! warning ""
	Il manque à l'inverse un détecteur de spam, tel que SpamAssassin.

## Utilisation

Pour se créer un compte, il suffit de cliquer sur "Première connexion" et de rentrer son adresse mail. Sympa envoie ensuite un mail à cette adresse (c'est son but) contenant un lien pour définir son mot de passe. Il est à noter que tous les comptes sont synchronisés entre les différentes URL.

Toute personne connectée peut demander à créer une liste. Il existe différents types de liste :

  * Liste confidentielle
    * Abonnement / désabonnement interdits. Seuls les propriétaires de la liste peuvent ajouter des membres.
    * archives privées
    * Liste visible aux seuls abonnés
    * Seuls abonnés peuvent poster des messages. LES AUTRES MESSAGES SONT REJETÉS SANS NOTIFICATION.
  * Liste de discussion publique
    * archives publiques
    * seuls les abonnés peuvent poster des messages
  * Liste de type hotline
    * droit de poster des messages ouvert à tous
    * archives privées
    * abonnements contrôlés
  * Paramétrage adapté à une newsletter aux formats Text/plain et HTML
    * liste publique et modérée
    * les adresses des abonnés sont protégées (contre le spam)
    * le format de réception par défaut est HTML
  * Liste pour l'Intranet
  * Paramétrage adapté à une newsletter
    * liste publique et modérée
    * les adresses des abonnés sont protégées (contre le spam)
  * Paramétrage pour un groupe de travail
    * seuls les abonnés peuvent poster des messages
    * archives privées
    * abonnements contrôlés
  * Liste de type forum web
    * Les messages peuvent être consultés par mail (abonnement) ou par l'interface web

La liste est ensuite soumise à validation d'un listmaster. Le listmaster décide enfin d'accepter la liste ou non.

Une liste peut comporter des administrateurs/modérateurs.

Les archives des mails sont enregistrées (selon paramètres) et peuvent être consultées par qui en a le droit, selon le type de liste.

L'interface est plutôt intuitive, et permet de gérer facilement les différents paramètres et les adhésions.

À venir : la création automatique de listes :)

