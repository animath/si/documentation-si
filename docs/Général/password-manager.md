# Gestionnaire de mots de passe

Un gestionnaire de mots de passe permet de stocker ses mots de passe de manière sécurisée sans avoir à les retenir. Le gestionnaire utilisé sur le serveur est *cranspasswords* ([Voir sur Gitlab](https://gitlab.crans.org/nounous/cranspasswords)), un logiciel développé par le [*Cr@ns*](https://crans.org), association étudiante de réseaux à l'ENS Paris-Saclay.

Ce gestionnaire de mots de passe offre la possibilité d'avoir des mots de passe chiffrés par clés GPG (et n'apparaissent par conséquent pas en clair sur le serveur), tout en permettant de partager certains mots de passe à certains groupes de personnes, appelés *rôles*.

## Établissement d'une clé GPG

Afin de pouvoir chiffrer et déchiffrer les mots de passe, chaque utilisateur doit avoir sa clé GPG. Il suffit pour cela d'exécuter la commande `gpg --full-generate-key`, après avoir installé le paquet GPG avec son gestionnaire de paquets favoris.

Les informations demandées sont :

* Le nom complet de la personne à authentifier
* L'adresse mail
* Un commentaire sur l'identité de personne
* Le type de clés
* La taille de la clé privée (en octets)
* La durée de vie de la clé avant que celle-ci n'expire.

Une phrase de passe vous sera demandée. Celle-ci doit être **extrêmement robuste** : si elle s'avère être compromise, la sécurité de votre coffre est compromise. Ne pas hésitez à faire des phrases très longues, que vous pourrez néanmoins mémoriser facilement (il ne s'agit pas de la noter quelque part).

Vous pouvez lister vos clés disponibles en exécutant `gpg --list-keys`.

Il est possible d'ajouter plusieurs identités à une même clé. Il suffit pour cela de lancer la commande `gpg --edit-key <ident>`, puis d'exécuter `adduid`. Nom, adresse mail et commentaire seront alors à nouveau demandés.

La clé est désormais créée. Vous pouvez ensuite récupérer son empreinte en exécutant `gpg --fingerprint <ident>`. C'est cette empreinte qui permettra de chiffrer vos mots de passe et contrôler votre identité.

## Configuration de cranspasswords

Cranspasswords réside sur le principe suivant :

* Le serveur contient l'intégralité des mots de passe, mais chiffrés. Lui-même ne sait pas les déchiffrer, ce qui évite le risque de fuite de mots de passe. Les mots de passe sont regroupés par *rôles*. Un *rôle* est un ensemble d'utilisateurs, caractérisés par leur pseudo et leur empreinte GPG.

* Le client a sa propre instance de cranspasswords sur sa machine. Lorsque que le client veut un mot de passe, il le demande au serveur, et celui-ci lui répond si et seulement si le pseudo du client appartient à un rôle qui connaît le mot de passe. Le serveur lui envoie ensuite le mot de passe chiffré. Le client, armé de sa clé privée, va pouvoir déchiffrer le mot de passe.

### Le serveur

On commence par cloner le serveur quelque part :

```
mkdir -p /srv/sources/tfjmpasswords && cd /srv/sources/tfjmpasswords
git clone https://gitlab.crans.org/nounous/cranspasswords.git .
git checkout dev	# La branche master est en retard
```

Cranspasswords offre la possibilité de changer le nom du programme. Il suffit pour cela de définir la variable d'environnement `COMMAND_NAME`, qui vaut par défaut `cranspasswords` :

```
export COMMAND_NAME=tfjmpasswords
```

Il suffit d'ensuite d'installer le serveur :

```
pip install -e .
```

L'instruction `-e` permet d'installer le paquet par liens symboliques, ce qui simplifiera la mise à jour via `git pull`, le projet étant encore en beta stable.

Le serveur est ensuite créé. Il suffit d'exécuter `tfjmpasswords-server` pour s'assurer de son existence (le message d'erreur est normal, il n'est pas prévu d'intéragir directement avec). Il reste désormais à le configurer. Pour cela, on peut commencer par copier la configuration d'exemple : 

```
sudo mkdir /etc/tfjmpasswords
sudo cp docs/serverconfig.example.py /etc/tfjmpasswords/serverconfig.py
sudo chown root:root /etc/tfjmpasswords/serverconfig.py
```

Le fichier `/etc/tfjmpasswords/serverconfig.py` contient alors la configuration serveur, c'est-à-dire l'ensemble des rôles principalement. Les options importantes :

* `cmd_name` : le nom de la commande, correspond à la valeur `COMMAND_NAME` précédemment définie. Mettre à `tfjmpasswords`.
* `READONLY` : pouvoir modifier des mots de passe peut être utile, laisser à `False`.
* `KEYS` : dictionnaire qui à chaque nom d'utilisateur associe l'adresse e-mail d'une identité ainsi que son empreinte GPG. C'est ici qu'il faut rentrer les empreintes de chacune des personnes qui doivent avoir un accès au gestionnaire de mots de passe.
* `ROLES` : dictionnaire qui à toute clé de type chaîne de caractère `role` va associer une liste de pseudos. Cela indique qui sont les personnes dans quels rôles. Pour les personnes ayant un droit d'écriture, ie. de pouvoir créer/modifier/supprimer des mots de passe, il faut également rajouter un rôle se terminant par `-w`.

Le serveur est désormais correctement configuré :)

### Le client

Le client s'installe non pas sur la machine distante, mais sur l'ordinateur propre de chaque personne. L'installation est similaire à celle du serveur :

```
mkdir tfjmpasswords && cd tfjmpasswords
git clone https://gitlab.crans.org/nounous/cranspasswords.git .
git checkout dev
export COMMAND_NAME=tfjmpasswords
pip install -e --user .
```

La configuration va ensuite différer. Encore une fois, on peut copier la configuration de base :

```
mkdir ~/.config/tfjmpasswords
cp docs/clientconfig.example.ini ~/.config/tfjmpasswords/clientconfig.ini
```

Et modifier ce dernier fichier. Ce fichier doit contenir la liste de tous les serveurs auxquels le gestionnaire peut se connecter. Celui s'appelant `[DEFAULT]` sera choisi par défaut.

Par exemple :

```
[DEFAULT]
host=tfjm.org
remote_cmd=/usr/local/bin/tfjmpasswords-server
```

Il est à noter qu'il doit être possible de se connecter en SSH à l'hôte sans mot de passe. Privilégier les clés :) L'accès au serveur d'Animath n'est de toute façon pas possible sans clé. Le fichier `~/.ssh/config` doit exister.

La configuration est déjà terminée, le gestionnaire est prêt à être utilisé :)

### Utilisation

L'aide peut être affichée en faisant un `tfjmpasswords --help` :

```
usage: tfjmpasswords [-h] [-v] [-q] [-s SRV] [--no-clipboard] [-f] [--drop-invalid] [--roles [ROLES]]
                     [--view | -e | --remove | -l | -r | --check-keys | --update-keys | --list-roles | --list-servers | --recrypt]
                     [filename]

Gestion de mots de passe partagés grâce à GPG.

positional arguments:
  filename              nom du fichier à lire ou éditer

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         mode verbeux, multiplier l'option -v pour augmenter
  -q, --quiet           mode silencieux: cache les erreurs, ignore la verbosité
  -s SRV, --server SRV  sélectionne un autre server que DEFAULT
  --no-clipboard        n'essaie pas de stocker le mot de passe dans le presse papier
  -f, --force           ne pas demander confirmation
  --drop-invalid        avec --force, ignore les clés en lesquels on n'a pas confiance sans confirmation.
  --roles [ROLES]       spécifier pour quels rôles chiffrer (défaut à tous les rôles, ou ne les change pas si édition)

actions:
  --view                lit le fichier (défaut)
  -e, --edit            édite (ou crée) le fichier
  --remove              efface le fichier
  -l, --list            liste les fichiers
  -r, --restore         restaure les fichiers corrompus
  --check-keys          vérifie les clés
  --update-keys         met à jour les clés
  --list-roles          liste les rôles existants
  --list-servers        liste les serveurs
  --recrypt             rechiffre tous les fichiers ayant un rôle listé dans --roles
```

Pour créer/modifier un mot de passe pour l'identifiant `IDENT`, il suffit de taper `tfjmpasswords -e IDENT`. Par défaut, le mot de passe est accessible à tous les rôles, il suffit de rajouter `--roles role1,role2,...,rolen` pour spécifier les rôles.

Un terminal vim va ensuite s'ouvrir. En face de `pass: `, il suffit de préciser le mot de passe. Une ligne `login: LOGIN` peut éventuellement être rajoutée pour préciser l'identifiant.

Pour récupérer un mot de passe, taper `tfjmpasswords IDENT`. Le mot de passe va être copié dans le presse-papier, sans apparaître en clair. Une fois le mot de passe utilisé, taper `Entrée` réinitialise le mot de passe.
