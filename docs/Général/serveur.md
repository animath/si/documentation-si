# Animath - Général

## L'architecture du serveur

Tous les services des actions d'Animath sont regroupés sur un unique serveur, hébergé chez Scaleway. Les spécificités de la machine seront précisées une fois que la migration des services aura réellement été effectuée.

Le serveur tourne avec [Docker](docker), avec [Traefik](traefik) pour reverse-proxy. Il est agencé de la façon suivante :

  * Toutes les données du serveur se trouvent dans le dossier `/srv`.
  * Dans le dossier `/srv` se trouvent un sous-dossier par action, en plus d'un dossier « Général ».
  * Dans chaque sous-dossier, on a l'architecture suivante :
```
.
├── console
├── data
├── secrets
├── sources
├── upgrade
└── docker-compose.yml
```
  * Ces dossiers sont agencés de la manière suivante, par ordre d'importance :
    * Seul le fichier `docker-compose.yml` se trouve à la racine. Voir [Docker](docker) pour plus d'informations sur Docker. C'est ici qu'on exécute toutes les commandes `docker-compose`.
    * Le dossier `sources` contient les sources des images à construire, qui ne sont pas répertoriées dans Dockerhub.
    * Le dossier `data` contient tous les volumes des services, constitués soit de données soit de fichiers de configuration. Attention : les permissions peuvent parfois être à gérer manuellement, voir la page d'un service pour voir quelles permissions doivent avoir les volumes.
    * Le dossier `secrets` contient les fichiers des variables d'environnement pour chaque service. On s'assurera que les permissions des fichiers à l'intérieur ne dépassent pas `0660`, voire `0640`. La permission `0600` n'est pas suffisante afin de ne pas nécessiter d'être super-utilisateur pour lancer un service Docker.
    * Le dossier `console` quelques petits scripts qui servent de raccourcis pour se glisser dans la console d'un service (c'est long d'écrire `docker-compose exec <service> bash` ...)
    * Enfin, le dossier `upgrade` contient des scripts qui, s'ils sont bien écrits, permettent de mettre à jour les différents services, d'une part en mettant à jour les sources, d'autre part les données si cela est possible en ligne de commande.

À noter que le temps de la migration des services, l'architecture est largement susceptible d'évoluer.

## Installation et configuration du serveur

Si tous les services d'Animath sont centralisés sur une seule et même grosse machine, ils restent néanmoins compartimentés par action comme décrit dans son architecture. C'est pourquoi, pour chaque action, on dispose d'un groupe d'utilisateurs UNIX. Être dans ce groupe permet de lire et d'écrire dans le dossier de l'action.

Afin de justement faire en sorte qu'automatiquement un nouveau fichier créé donne le droit d'écriture au groupe (ce qui n'est pas le comportement par défaut de UNIX), il faut exécuter la commande :

```bash
umask 002
```

Cela permet de créer les fichiers avec des permissions `0664` pour les fichiers et `0775` pour les dossiers par défaut, c'est-à-dire lecture pour tous, écriture pour le propriétaire et le groupe uniquement (plus exécution pour les dossiers). Pour que cette commande soit exécutée à chaque connexion sur le serveur, on ajoute la ligne `umask 002` dans les fichiers `/etc/profile.d/42-umask.sh` (qu'on crée en permission 0755 en tant que `root`) et `/etc/zsh/zshrc`.

Au besoin, on peut rajouter le droit d'écriture aux groupes si ce n'est pas déjà fait :

```
sudo chmod -R g+w /srv
```

Attention toutefois à enlever le droit aux fichiers les plus protégés et aux fichiers dans des volumes.

De plus, on veut que les fichiers et dossiers créés dans le dossier `action` appartiennent au groupe `action`. Pour cela, on peut donner l'attribut spécial `s` :

```bash
sudo chmod g+s /srv/action
```

qui a exactement le comportement souhaité. Pour le faire récursivement dans tous les sous-dossiers :

```bash
sudo find /srv/action -type d -exec chmod g+s {} \;
```

On s'assure enfin que chaque personne concernée est dans le groupe de son action :

```bash
sudo adduser <utilisateur> <groupe>
```

Pour les administrateurs de la machine complète, qui auront le droit d'exécuter `sudo`, il faut en plus se placer dans le groupe `wheel` (nom à revoir sans doute) :

```bash
sudo adduser <utilisateur> wheel
```

Il faut redémarrer le shell pour que les modifications prennent effet. Le groupe `wheel` doit être ajouté aux sudoers :

```bash
sudo visudo
%wheel		ALL=(ALL:ALL) ALL			<-- ligne à ajouter ou décommenter
```

Enfin, afin de pas nécessiter d'être superutilisateur pour exécuter une commande Docker, on pensera à s'ajouter dans le groupe `docker` :

```bash
sudo adduser <utilisateur> docker
```

Les permissions sont ensuite normalement bien configurées :) Néanmoins, on rappelle qu'un passage manuel sur les permissions des volumes Docker peut s'imposer.

!!! danger ""
	L'installation de la nouvelle machine étant en cours, cette documentation est susceptible d'évoluer.

## Ajout d'un nouvel administrateur

On commence par lui créer son compte :

```bash
sudo adduser <utilisateur>
```

Ensuite, on l'ajoute dans tous les groupes auxquels il doit être, en n'oubliant pas le groupe `docker` pour tout le monde et `wheel` pour les administrateurs :

```bash
sudo adduser <utilisateur> <groupe>
```

Enfin, on ajoute la [clé publique](ssh) de l'utilisateur :

```bash
mkdir /home/<utilisateur>/.ssh
echo "<clé publique>" > /home/<utilisateur>/.ssh/authorized_keys
chown -R <utilisateur>:<utilisateur> /home/<utilisateur>/.ssh
chmod -R 0600 /home/<utilisateur>/.ssh
```

L'utilisateur peut désormais se connecter :)
