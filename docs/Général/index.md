# Services généraux Animath

Ici se trouve l'ensemble des services accessibles à toutes les actions d'Animath :

## Services accessibles aux actions

* [Etherpad](etherpad) ([pad.animath.live](https://pad.animath.live))
* [Hedgedoc](hedgedoc) ([hedgedoc.animath.live](https://hedgedoc.animath.live))
* [Sympa](sympa)

## Services indispensables au bon fonctionnement du serveur
* [Docker](docker)
* [Traefik](traefik)
* [SSH](ssh)

On trouvera également des informations à propos de l'[architecture du serveur](serveur).
