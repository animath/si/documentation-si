# Hedgedoc

Hedgedoc est une plateforme d'édition collaborative libre très légère. Elle permet de travailler à plusieurs sur un même document pouvant être proprement mis en forme et de les partager. La principale différence avec [Etherpad](etherpad) est dans la simplicité et le formattage des documents.

Une instance est disponible sur [notes.animath.live](https://notes.animath.live) (également accessible depuis [hedgedoc.animath.live](https://hedgedoc.animath.live) ou [codimd.animath.live](https://codimd.animath.live)).

La documentation de l'application est présente sur [hedgedoc.org](https://hedgedoc.org/).

## Installation

L'installation se fait grâce à son image Docker officielle.

Sa configuration dans le fichier `docker-compose.yml` est standard :

```yaml
 hedgedoc:
    image: quay.io/hedgedoc/hedgedoc:alpine
    restart: always
    links:
      - postgres
    volumes:
      - /srv/Animath/data/hedgedoc:/hedgedoc/public/uploads
    env_file:
      - /srv/Animath/secrets/hedgedoc.env
    networks:
      - global
    labels:
      - "traefik.http.routers.hedgehoc.rule=Host(`notes.animath.live`)"
      - "traefik.http.routers.hedgehoc.entrypoints=websecure"
      - "traefik.http.routers.hedgehoc.tls.certresolver=mytlschallenge"
      - "traefik.http.services.hedgehoc.loadbalancer.server.port=3000"
```

Une unique variable d'environnement est à définir, permettant de se connecter à la base de données PostgreSQL :

```
CMD_DB_URL=postgres://hedgedoc:password@postgres:5432/hedgedoc
```
