# Etherpad

Etherpad est une plateforme d'édition collaborative libre très légère. Elle permet de travailler à plusieurs sur un même brouillon très facilement et de les partager.

Une instance est disponible sur [pad.animath.live](https://pad.animath.live).

La documentation de l'application est présente sur [etherpad.org](https://etherpad.org).

## Installation

L'installation se fait grâce à son image Docker officielle.

Sa configuration dans le fichier `docker-compose.yml` est standard :

```yaml
  etherpad:
    build:
      context: https://github.com/ether/etherpad-lite.git
      args:
        - "ETHERPAD_PLUGINS=ep_adminpads2 ep_align ep_delete_after_delay ep_delete_empty_pads ep_comments_page ep_font_size ep_headings2"
    restart: always
    links:
      - postgres
    env_file:
      - /srv/Animath/secrets/etherpad.env
    volumes:
      - /srv/Animath/data/etherpad/settings.json:/opt/etherpad-lite/settings.json
    networks:
      - global
    labels:
      - "traefik.http.routers.etherpad.rule=Host(`pad.example.com`)"
      - "traefik.http.routers.etherpad.entrypoints=websecure"
      - "traefik.http.routers.etherpad.tls.certresolver=mytlschallenge"
      - "traefik.http.services.etherpad.loadbalancer.server.port=9001"
```

Les variables d'environnement contiennent les paramètres d'installation de l'application. La documentation complète est disponible sur [Github](https://github.com/ether/etherpad-lite/blob/develop/doc/docker.md).

```
TITLE=Etherpad - Animath
ADMIN_PASSWORD=
DB_TYPE=
DB_HOST=
DB_PORT=
DB_NAME=
DB_USER=
DB_PASS=
PAD_OPTIONS_LANG=fr
DISABLE_IP_LOGGING=true  # Pour des connexions anonymes
TRUST_PROXY=true         # Pour suivre le reverse-proxy
```

Les plugins sont à installer à la construction de l'image, dans l'argument `ETHERPAD_PLUGINS`, comme indiqué ci-dessus. La liste exhaustive des plugins est disponible sur [static.etherpad.org](https://static.etherpad.org).

Les paramètres des plugins sont à définir dans le fichier `settings.json`, et ne peuvent pas être définies par des variables d'environnement. Avant de volumer le fichier de configuration, on peut lancer une première fois Etherpad, puis récupérer le fichier :

```bash
sudo docker cp animath_etherpad_1:/opt/etherpad-lite/settings.json /srv/Animath/data/etherpad/settings.json
```

On peut ensuite modifier le fichier à souhait et le volumer, comme dans la configuration ci-dessus. Il est préférable de ne pas toucher aux options de Etherpad qui peuvent être définies par des variables d'environnement, mais uniquement d'ajouter la configuration des plugins.

Pour le plugin `ep_delete_after_delay` permettant de laisser les pads expirer au bout d'un certain temps, on ajoute la configuration :

```json
  /* Configure automatic deletion */
  "ep_delete_after_delay": {
    "delay": 2419200, // one month, in seconds
    "loop": true,
     "loopDelay": 43200, // twelve hours, in seconds
     "deleteAtStart": true
  }
```
