# Bac à sable

Un deuxième serveur est disponible pour Animath, indépendant du gros premier. Il a un rôle de bac à sable et peut servir pour former les nouveaux administrateurs ou bien pour tester des installations.

Il est accessible sur [animath.live](https://animath.live).

Plus de spécificités et des détails sur son utilisation arriveront ultérieurement.
